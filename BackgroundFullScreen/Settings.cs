﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Windows.Forms;
using static System.Windows.Forms.ListView;

namespace BackgroundFullScreen
{
    public partial class Settings : Form
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool OpenProcessToken(IntPtr ProcessHandle, uint DesiredAccess, out IntPtr TokenHandle);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);

        private Background _background;

        public Settings()
        {
            InitializeComponent();
            _background = null;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            SetLastColorUsed();
            LoadProcesses();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (_background == null)
            {
                Application.Exit();
            }
        }

        #region Private Methods

        private void SetLastColorUsed()
        {
            object lastColorUsed = Properties.Settings.Default["LastColorUsed"];
            Color color = Color.Black;

            if (lastColorUsed == null || ((Color)lastColorUsed).IsEmpty)
            {
                Properties.Settings.Default["LastColorUsed"] = color;
                Properties.Settings.Default.Save();
            }
            else
            {
                color = (Color)lastColorUsed;
            }

            label1.BackColor = color;
        }

        private void LoadProcesses()
        {
            string currentUser = Environment.UserName;

            listView1.Items.Clear();

            Process[] processes = System.Diagnostics.Process.GetProcesses();
            ImageList imageList = new ImageList();

            foreach (Process p in processes)
            {
                string processName = p.MainWindowTitle; //p.ProcessName;
                string processUser = GetProcessUser(p);

                if (p.Responding && processName != this.Text && processUser == currentUser && !string.IsNullOrEmpty(processName))
                {
                    try
                    {
                        Bitmap processIcon = Icon.ExtractAssociatedIcon(p.MainModule.FileName).ToBitmap();
                        imageList.Images.Add(processIcon);
                        listView1.Items.Add(p.Id.ToString(), processName, imageList.Images.Count - 1);
                    }
                    catch
                    {
                        listView1.Items.Add(p.Id.ToString(), processName, -1);
                    }
                }
            }

            listView1.SmallImageList = imageList;

            if (listView1.Items.Count > 8)
            {
                listView1.Columns[0].Width = listView1.Width - 17;
            } else
            {
                listView1.Columns[0].Width = listView1.Width;
            }
        }

        private static string GetProcessUser(Process process)
        {
            IntPtr processHandle = IntPtr.Zero;
            try
            {
                OpenProcessToken(process.Handle, 8, out processHandle);
                WindowsIdentity wi = new WindowsIdentity(processHandle);
                string user = wi.Name;
                return user.Contains(@"\") ? user.Substring(user.IndexOf(@"\") + 1) : user;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (processHandle != IntPtr.Zero)
                {
                    CloseHandle(processHandle);
                }
            }
        }

        #endregion Private Methods
        
        private void label1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                label1.BackColor = colorDialog1.Color;
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            SelectedListViewItemCollection selected = listView1.SelectedItems;

            if (selected.Count > 0)
            {
                int pid = Convert.ToInt32(selected[0].Name);

                Process p = System.Diagnostics.Process.GetProcessById(pid);

                if (p != null && !p.HasExited)
                {
                    _background = new Background(label1.BackColor, p);
                }
            }
            else
            {
                _background = new Background(label1.BackColor);
            }

            Properties.Settings.Default["LastColorUsed"] = label1.BackColor;
            Properties.Settings.Default.Save();

            _background.FormBorderStyle = FormBorderStyle.None;
            _background.WindowState = FormWindowState.Maximized;
            _background.Show();

            this.Close();
        }

        private void btnReloadProcesses_Click(object sender, EventArgs e)
        {
            LoadProcesses();
        }
    }
}
