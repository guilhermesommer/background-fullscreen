﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace BackgroundFullScreen
{
    public partial class Background : Form
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        Process _process;
        private IntPtr _oldWindowHandle;
        private int _selectedPID;

        public Background()
        {
            InitializeComponent();

            _process = null;
            _oldWindowHandle = IntPtr.Zero;
            _selectedPID = 0;
        }

        public Background(Color color) : this()
        {
            this.BackColor = color;
        }

        public Background(Color color, Process process) : this()
        {
            this.BackColor = color;
            this._process = process;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                return CloseApplication();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            if (_process != null)
            {
                _selectedPID = _process.Id;
                
                Thread.Sleep(50);
                _oldWindowHandle = SetParent(_process.MainWindowHandle, this.Handle);
            }
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            CloseApplication();
        }

        private bool CloseApplication()
        {
            this.Close();
            Application.Exit();
            return true;
        }

        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
